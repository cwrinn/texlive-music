#!/bin/bash
command="$@"
if [[ -z "${command}" ]];then
  if [[ -z "${MAIN_FILE}" ]];then
    echo "Seriously? You didn't set MAIN_FILE..."
    echo "Trying to divine what you are wanting me to render..."

    texfiles=($(ls *.tex))

    if (( "${#texfiles[@]}" > 1 ));then
      echo "I see more than one texfiles, this is confusing me. [=].[=];;"
      exit 1
    elif (( "${#texfiles[@]}" == 1 ));then
      echo "Using ${texfiles[0]}."
      MAIN_FILE="${texfiles[0]}"
    else
      echo "Are you pulling my leg? I see nothing to do."
      exit 4
    fi
  elif [[ ! -f ${MAIN_FILE} ]];then
    echo "There's no ${MAIN_FILE}, wutchu talkin bout."
    exit 5
  fi
  texcmd="lualatex --shell-escape"

  command="${texcmd} ${MAIN_FILE}"
fi
exec ${command}
